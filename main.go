package main

import (
	"cellulargamev3/funcs"
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

// structs (i h8 my kife)
type ClientRequest struct {
	Type string                 `json:"type"`
	Data map[string]interface{} `json:"data"`
}

type ClientResponse struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

// vars
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	http.HandleFunc("/", handler)
	go log.Fatal(http.ListenAndServe(":361", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	//var OldState []structs.Cell = nil

	for {
		//fmt.Println("test")

		creq := &ClientRequest{}
		cresp := &ClientResponse{}

		_ = conn.ReadJSON(&creq)

		switch creq.Type {
		case "spawnFood":
			cresp.Data = ToInterface(funcs.SpawnFood())

		case "getFood":
			cresp.Data = ToInterface(funcs.GetFood())

		case "getCells":
			cresp.Data = ToInterface(funcs.GetCells())

		case "initCell":
			cresp.Data = ToInterface(funcs.InitCell(creq.Data["name"].(string)))

		case "changePos":
			var pos []int
			data := creq.Data["pos"].([]interface{})
			for _, item := range data {
				pos = append(pos, int(item.(float64)))
			}
			cresp.Data = ToInterface(funcs.ChangePos(int(creq.Data["id"].(float64)),  pos))

		case "changeSize":
			cresp.Data = ToInterface(funcs.ChangeSize(creq.Data["id"].(int), creq.Data["size"].(int)))

		case "delall":
			funcs.Delall()
			cresp.Data = nil

		case "eat":
			log.Println(creq)

			playerId := int(creq.Data["id"].(float64))
			mealId := int(creq.Data["mealId"].(float64))
			mealType := creq.Data["type"].(string)

			cresp.Data = ToInterface(funcs.Eat(playerId, mealId, mealType))

			log.Println(cresp)

		default:
			cresp.Data = nil

		}
		cresp.Type = creq.Type
		if creq.Type != "getCells" && creq.Type != "getFood" {
			log.Printf("New request of type %s", cresp.Type)
		}
		conn.WriteJSON(cresp)
	}
}

func ToInterface(rawData interface{}) interface{} {
	jsonData, _ := json.Marshal(rawData)
	var data interface{}
	_ = json.Unmarshal(jsonData, &data)
	return data
}
