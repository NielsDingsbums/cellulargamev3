package funcs

import (
	"cellulargamev3/structs"
	"log"
	"math/rand"
	"time"
)

// global stuff
var Cells []structs.Cell
var FoodItems []structs.Food

func SpawnFood() []structs.Food {
	valuesSrc := []int{5, 10, 20}
	rarities := []int{5, 4, 1}

	FoodItems = []structs.Food{}

	var values []int
	items := 50

	for i := 0; i < items; i++ {
		if len(values) < rarities[0] {
			values = append(values, valuesSrc[0])
		} else if len(values) < rarities[1]+rarities[0] {
			values = append(values, valuesSrc[1])
		} else {
			values = append(values, valuesSrc[2])
		}
	}
	for i := 0; i < len(values); i++ {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)

		var pos []int
		for i := 0; i < 2; i++ {
			s := rand.NewSource(time.Now().UnixNano())
			r := rand.New(s)
			pos = append(pos, r.Intn(2000)-1000)
		}

		var newItem structs.Food
		value := values[r1.Intn(len(values))]
		newItem = structs.Food{len(FoodItems), pos, value, true}

		FoodItems = append(FoodItems, newItem)
	}

	log.Println(FoodItems)

	return FoodItems
}

func GetFood() []structs.Food {
	return FoodItems
}

func InitCell(name string) structs.Cell {
	var NewCell structs.Cell
	var pos []int

	for i := 0; i < 2; i++ {
		s := rand.NewSource(time.Now().UnixNano())
		r := rand.New(s)
		pos = append(pos, r.Intn(2000)-1000)
	}

	NewCell = structs.Cell{len(Cells), name, true,30, 0, []structs.Cell{}, pos}

	Cells = append(Cells, NewCell)
	log.Printf("New Cell with id %d called  %s at %v", NewCell.Id, NewCell.Name, NewCell.Pos)

	return NewCell
}

func ChangeSize(id int, size int) []structs.Cell {
	Cells[id].Size += size
	log.Printf("The size of id %d ('%s') was changed by %d. The current size is %d", id, Cells[id].Name, size, Cells[id].Size)
	return Cells
}

func Delall() {
	Cells = []structs.Cell{}
	FoodItems = []structs.Food{}
}

func Eat(id int, mealId int, mealType string) []structs.Cell {
	if mealType == "food" {
		meal := &FoodItems[id]
		player := &Cells[id]

		size := player.Size
		ChangeSize(id, meal.Value);
		meal.Alive = false

		log.Printf("The player with id %d (%s) of size %d has eaten foodItem %d of valu e %d. The new size of player %d is now %d", player.Id, player.Name, size, meal.Id, meal.Value, player.Id, player.Size)
	} else {
		meal := &Cells[mealId]
		player := &Cells[id]

		sizes := []int{player.Size, meal.Size};
		ChangeSize(id, meal.Size)
		meal.Alive = false
		player.Meals = append(Cells[id].Meals, *meal)
		player.Kills += 1

		log.Printf("The player with id %d (%s) of size %d has eaten player %d (%s) of size %d. The new size of player %d is now %d", player.Id, player.Name, sizes[0], meal.Id, meal.Name, sizes[1], player.Id, player.Size)
	}
	return Cells
}

func GetCells() []structs.Cell {
	return Cells
}

func ChangePos(id int, pos []int) []structs.Cell {
	Cells[id].Pos = pos
	return Cells
}
